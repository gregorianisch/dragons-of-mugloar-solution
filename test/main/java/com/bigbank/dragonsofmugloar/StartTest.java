package main.java.com.bigbank.dragonsofmugloar;

import org.junit.Test;
import org.junit.Assert;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StartTest {

    @Test
    public void gameCycleCompleted() throws Exception {
        String numberOfFights = "1";
        String regex = "\\d+.\\d+(?=%)";
        double totalPercentage = 0.0;
        double expectedPercentage = 100.0;

        String [] argNumberOfFights = new String[]{numberOfFights};
        Start.main(argNumberOfFights);

        BufferedReader file = new BufferedReader(new FileReader("game_test.log"));
        String lastLine = "";
        String line;

        while ((line = file.readLine()) != null) {
            lastLine = line;
        }

        file.close();

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(lastLine);

        while (matcher.find()) {
            totalPercentage = totalPercentage + new Double(matcher.group());
        }

        Assert.assertEquals("Total percentage of battles should be 100", expectedPercentage, totalPercentage, 0);
    }

    @Test
    public void fullGameCycleCompleted() throws Exception {
        String numberOfFights = "50";
        String regex = "\\d+.\\d+(?=%)";
        double totalPercentage = 0.0;
        double expectedPercentage = 100.0;

        String [] argNumberOfFights = new String[]{numberOfFights};
        Start.main(argNumberOfFights);

        BufferedReader file = new BufferedReader(new FileReader("game_test.log"));
        String lastLine = "";
        String line;

        while ((line = file.readLine()) != null) {
            lastLine = line;
        }

        file.close();

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(lastLine);

        while (matcher.find()) {
            totalPercentage = totalPercentage + new Double(matcher.group());
        }

        Assert.assertEquals("Total percentage of battles should be 100", expectedPercentage, totalPercentage, 0);
    }

    @Test(expected = Exception.class)
    public void wrongArgumentProvided() throws Exception {
        String numberOfFights = "0";

        String [] argNumberOfFights = new String[]{numberOfFights};
        Start.main(argNumberOfFights);
    }

    @Test(expected = Exception.class)
    public void nullArgumentProvided() throws Exception {
        String numberOfFights = null;

        String [] argNumberOfFights = new String[]{numberOfFights};
        Start.main(argNumberOfFights);
    }

    @Test(expected = Exception.class)
    public void negativeArgumentProvided() throws Exception {
        String numberOfFights = "-1";

        String [] argNumberOfFights = new String[]{numberOfFights};
        Start.main(argNumberOfFights);
    }
}