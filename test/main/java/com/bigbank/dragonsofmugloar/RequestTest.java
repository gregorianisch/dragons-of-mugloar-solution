package main.java.com.bigbank.dragonsofmugloar;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class RequestTest {
    @Test
    public void getResponseToGameGetRequest() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com/api/game/");
        String response = request.sendGetRequest();

        JSONObject json = new JSONObject(response);

        Assert.assertTrue("Game id should be returned", json.has("gameId"));
    }

    @Test
    public void getResponseToWeatherGetRequest() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com/weather/api/report/222");
        String response = request.sendGetRequest();

        Assert.assertTrue("Game id should be returned", response.contains("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
    }

    @Test
    public void getResponseToPutRequest() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com/api/game/222/solution");

        JSONObject dragonSkills = new JSONObject();
        JSONObject dragonJson = new JSONObject();

        dragonSkills.put("clawSharpness", 7);
        dragonSkills.put("scaleThickness", 8);
        dragonSkills.put("wingStrength", 2);
        dragonSkills.put("fireBreath", 3);
        dragonJson.put("dragon", dragonSkills);

        request.setPutData(dragonJson.toString());

        String response = request.sendPutRequest();
        JSONObject json = new JSONObject(response);

        Assert.assertTrue("Game id should be returned", json.has("status"));
    }

    @Test(expected = Exception.class)
    public void getResponseToInvalidGetRequest() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com");
        request.sendGetRequest();
    }

    @Test(expected = Exception.class)
    public void getResponseToInvalidPutRequest() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com");
        request.sendPutRequest();
    }

    @Test(expected = Exception.class)
    public void getResponseToInvalidPutData() throws Exception {
        Request request = new Request("http://www.dragonsofmugloar.com/api/game/222/solution");
        request.setPutData("");
        request.sendPutRequest();
    }

}