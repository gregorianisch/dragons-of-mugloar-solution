package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

class Dragon {

    private static Logger log = Logger.getLogger(Dragon.class);
    private int clawSharpness;
    private int scaleThickness;
    private int wingStrength;
    private int fireBreath;
    private JSONObject skills;
    private String gameWeather;
    private String[][] knightSkills;

    public Dragon(String gameWeather, String[][] knightSkills) throws Exception {

        if (gameWeather.equals("")) {
            throw new Exception("Weather can't be empty");
        } else if (!gameWeather.matches("(normal|flood|dry|fog)")) {
            throw new Exception("Allowed weather is normal|flood|dry|fog");
        } else {
            this.gameWeather = gameWeather;
            this.knightSkills = knightSkills;
        }
    }

    JSONObject getSkills() throws Exception {
        setSkills();
        return skills;
    }

    private void setSkills() throws Exception {
        Map<String, String> knightDragonMap = new HashMap<>();

        knightDragonMap.put("armor", "clawSharpness");
        knightDragonMap.put("attack", "scaleThickness");
        knightDragonMap.put("agility", "wingStrength");
        knightDragonMap.put("endurance", "fireBreath");

        calculateSkills(knightDragonMap);

        JSONObject dragonSkills = new JSONObject();

        dragonSkills.put("clawSharpness", clawSharpness);
        dragonSkills.put("scaleThickness", scaleThickness);
        dragonSkills.put("wingStrength", wingStrength);
        dragonSkills.put("fireBreath", fireBreath);

        skills = new JSONObject();
        skills.put("dragon", dragonSkills);

        log.info("Dragon skills: " + skills.toString(4));
    }

    /**
     *  Knight armor is mapped with dragon clawSharpness (attack).
     *  Knight attack is mapped with dragon scaleThickness (armor)
     *  to ensure dragon win.
     *
     *  Loop over knight array, get mapped dragon key and calculate value depending
     *  on knight skill with greatest value.
     *  Add + 2 to knight skill with greatest value and subtract -1 from two of the next
     *  decreasing skills.
     */
    private void calculateSkills(Map<String, String> knightDragonMap) {

        // If weather is normal make calculations, else use predefined values.
        if (gameWeather.equals("normal")) {

            int knightSkillValue;
            String knightSkillKey;

            for (int i = 0; i < knightSkills.length; i++) {

                knightSkillKey = knightSkills[i][0];
                knightSkillValue = new Integer(knightSkills[i][1]);

                switch (i) {
                    case 0:
                        setDragonSkill(knightDragonMap.get(knightSkillKey),knightSkillValue + 2);
                        break;
                    case 1:
                        setDragonSkill(knightDragonMap.get(knightSkillKey), knightSkillValue - 1);
                        break;
                    case 2:
                        setDragonSkill(knightDragonMap.get(knightSkillKey), knightSkillValue - 1);
                        break;
                    case 3:
                        setDragonSkill(knightDragonMap.get(knightSkillKey), knightSkillValue);
                        break;
                }
            }
        } else if (gameWeather.equals("flood")) {
            setDragonSkill("clawSharpness", 10);
            setDragonSkill("scaleThickness", 5);
            setDragonSkill("wingStrength", 5);
            setDragonSkill("fireBreath", 0);
        } else if (gameWeather.equals("dry") || gameWeather.equals("fog")) {
            setDragonSkill("clawSharpness", 5);
            setDragonSkill("scaleThickness", 5);
            setDragonSkill("wingStrength", 5);
            setDragonSkill("fireBreath", 5);
        }
    }

    private void setDragonSkill(String skillName, int skillValue) {
        switch (skillName) {
            case "clawSharpness":
                clawSharpness = skillValue;
                break;
            case "scaleThickness":
                scaleThickness = skillValue;
                break;
            case "wingStrength":
                wingStrength = skillValue;
                break;
            case "fireBreath":
                fireBreath = skillValue;
                break;
        }
    }
}
