package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import org.json.*;

class Fight {

    private static Logger log = Logger.getLogger(Fight.class);

    private int gameId;
    private JSONObject dragonSkills;

    public Fight(int gameId, JSONObject dragonSkills) {
        this.gameId = gameId;
        this.dragonSkills = dragonSkills;
    }

    void start() throws Exception {

        String fightEndpoint = "http://www.dragonsofmugloar.com/api/game/" + gameId + "/solution";
        JSONObject result;

        Request request = new Request(fightEndpoint);
        request.setPutData(dragonSkills.toString());

        try {
            result = new JSONObject(request.sendPutRequest());
        } catch (Exception e) {
            throw new Exception("Can't get JSON from PUT response.");
        }

        String status = result.getString("status");

        if (status.equals("Victory")) {
            Statistics.incrementVictoriesCounter();
        }

        log.info("Fight status: " + status + " Fight message: " + result.getString("message"));
    }
}
