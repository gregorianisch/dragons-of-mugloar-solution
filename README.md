# Description

Solution for [Dragons Of Mugloar](http://www.dragonsofmugloar.com) game written in Java.

* Initiates new game through turning to game API, gets game ID and knight skills
* Gets weather xml and extracts current weather condtition from it
* Calculates dragon skills values depending on knight skills values and weather condition
* Puts dragon into fight!

# Setup in [IntelliJ IDEA](https://www.jetbrains.com/idea/)

* Import a new Java Project
* Mark `/src` as Sources in Module Settings (optional)
* Mark `/src/resources` as Resources in Module Settings (optional)
* Mark `/test` as Tests in Module Settings (optional)
* Mark `/test/resources` as Test Resoources in Module Settings (optional)
* Run `Start.java` with one argument that defines number of fights and which should be an integer greater than 0

# Project Structure

* `/lib` contains all used libraries
* `/src/main/java/..` contains source code
* `/src/main/resources/log4j.properties` contains log4j settings
* `/test/main/java/..` contains JUnit tests
* `/test/main/resources/log4j.properties` contains log4j settings for tests
* `/*.log` log files are located in root directory

# Features

## JUnit Tests

## Logs

* Logging is resolved with log4j
* Logs are separated for unit tests and for application itself
* Application has two kinds of logs:
  * `game.log` contains general information
  * `system.log` contains extended requests and response information

### Example of `game.log`

```
2017-01-18 00:29:23.895 [INFO] bigbank.dragonsofmugloar.Game:29 - New game initiated with ID 4753731
2017-01-18 00:29:24.002 [INFO] bigbank.dragonsofmugloar.Weather:34 - Weather for game with ID 4753731 is: normal
2017-01-18 00:29:24.007 [INFO] bigbank.dragonsofmugloar.Knight:29 - Knight data: {
    "armor": 8,
    "attack": 1,
    "name": "Sir. Herbert Doyle of Prince Edward Island",
    "agility": 4,
    "endurance": 7
}
2017-01-18 00:29:24.010 [INFO] bigbank.dragonsofmugloar.Dragon:56 - Dragon skills: {"dragon": {
    "fireBreath": 6,
    "wingStrength": 3,
    "clawSharpness": 10,
    "scaleThickness": 1
}}
2017-01-18 00:29:24.073 [INFO] bigbank.dragonsofmugloar.Fight:38 - Fight status: Victory Fight message: Dragon was successful in a glorious battle
2017-01-18 00:29:24.075 [INFO] bigbank.dragonsofmugloar.Statistics:22 - Total fights completed: 1 with win rate: 100.0% (1 wins) out of which: 0.0% (0 storms) were UNWINNABLE storms.
```