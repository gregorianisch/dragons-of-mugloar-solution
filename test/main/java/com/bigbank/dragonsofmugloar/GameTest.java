package main.java.com.bigbank.dragonsofmugloar;

import org.junit.Assert;
import org.junit.Test;

public class GameTest {
    @Test
    public void gameIdNot0() throws Exception {
        Game game = new Game();
        game.start();
        Assert.assertTrue("Game Id should be greater than 0", game.getGameId() > 0);
    }

    @Test
    public void gameJsonNotNull() throws Exception {
        Game game = new Game();
        game.start();
        Assert.assertTrue("Game JSON can't be null", game.getGameJson() != null);
    }

    @Test
    public void parseGameIdFromJson() throws Exception {
        Game game = new Game();
        game.start();
        int gameId = game.getGameJson().getInt("gameId");
        Assert.assertEquals("Variable gameId should be equal to parsed gameId from JSON", game.getGameId(), gameId);
    }

}