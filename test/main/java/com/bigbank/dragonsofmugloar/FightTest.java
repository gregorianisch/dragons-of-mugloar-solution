package main.java.com.bigbank.dragonsofmugloar;

import org.json.JSONObject;
import org.junit.Test;

public class FightTest {

    @Test(expected = Exception.class)
    public void invalidJson() throws Exception {

        String invalidJson = "{\n" +
                "    \"dragon\": \n" +
                "        \"scaleThickness\": 5,\n" +
                "        \"clawSharpness\": 5,\n" +
                "        \"wingStrength\": 5,\n" +
                "        \"fireBreath\": 5\n" +
                "    }\n" +
                "}";

        JSONObject json = new JSONObject(invalidJson);

        Fight fight = new Fight(222, json);
        fight.start();
    }

    @Test(expected = Exception.class)
    public void inputArgumentIsNull() throws Exception {

        Fight fight = new Fight(222, null);
        fight.start();
    }
}