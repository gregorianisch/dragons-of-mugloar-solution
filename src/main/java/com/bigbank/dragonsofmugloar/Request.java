package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.*;
import com.sun.jdi.InvalidTypeException;
import java.util.*;

class Request {

    private static Logger log = Logger.getLogger(Request.class);
    private String requestEndpoint;
    private String putJSONData;
    private List allowedContentTypes = Arrays.asList("application/json", "application/xml");

    public Request(String requestEndpoint) {
        if (requestEndpoint == null) {
            throw new NullPointerException("Request endpoint is null.");
        } else {
            this.requestEndpoint = requestEndpoint;
        }
    }

    void setPutData(String putJSONData) throws Exception {
        if (putJSONData == null) {
            throw new NullPointerException("Data provided for PUT request is null.");
        } else if (putJSONData.equals("")) {
            throw new Exception("Put Json data can't be empty.");
        } else {
            this.putJSONData = putJSONData;
        }
    }

    String sendGetRequest() throws Exception {

        URL url = new URL(this.requestEndpoint);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();

        request.setRequestMethod("GET");
        request.setRequestProperty("User-Agent", "Mozilla/5.0");
        request.setRequestProperty("Accept-Language", "en-US,en;");
        String contentType = request.getContentType().split(";")[0].trim();

        if (!allowedContentTypes.contains(contentType)) {
            throw new InvalidTypeException("Content type of response is not allowed" + this.requestEndpoint);
        }

        if (request.getResponseCode() != HttpURLConnection.HTTP_OK)  {
            log.error("Response status: " + request.getResponseCode() + " for url: " + this.requestEndpoint);
            System.exit(0);
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(request.getInputStream()));
        String output;
        StringBuilder response = new StringBuilder();

        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();

        log.debug("Response for GET request to endpoint: " + this.requestEndpoint +
                  " is: " + response.toString());

        return response.toString();

    }

    String sendPutRequest() throws Exception {

        URL obj = new URL(this.requestEndpoint);
        HttpURLConnection request = (HttpURLConnection) obj.openConnection();

        request.setRequestMethod("PUT");
        request.setRequestProperty("User-Agent", "Mozilla/5.0");
        request.setRequestProperty("Accept-Language", "en-US,en;");
        request.setRequestProperty("Content-Type","application/json");
        request.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(request.getOutputStream());
        wr.writeBytes(this.putJSONData);
        wr.flush();
        wr.close();

        if (request.getResponseCode() != HttpURLConnection.HTTP_OK)  {
            log.error("Response status:" +  request.getResponseCode() + "for url: " + this.requestEndpoint);
            System.exit(0);
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(request.getInputStream()));
        String output;

        StringBuilder response = new StringBuilder();

        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();

        log.debug("Response for PUT request to endpoint: " + this.requestEndpoint +
                  " with data: " + this.putJSONData +
                  " is: " + response.toString());

        return response.toString();
    }
}

