package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;

class Start {

    private static Logger log = Logger.getLogger(Start.class);

    public static void main(String[] args) throws Exception {

        int fightCounter = 0;
        int numberOfFights;

        try {
            numberOfFights = new Integer(args[0]);
        } catch (NumberFormatException e) {
            log.error("Can't convert argument to integer.");
            throw new Exception("ERROR: can't convert argument to integer.");
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error("Argument which declares number of fights is missing.");
            throw new Exception("ERROR: argument which declares number of fights is missing.");
        }

        if (numberOfFights <= 0) {
            log.error("Argument can't be 0 or negative number.");
            throw new Exception("ERROR: argument can't be 0 or negative number.");
        }

        do try {

            Game game = new Game();
            game.start();

            Weather weather = new Weather(game.getGameId());
            String weatherForTheGame = weather.getWeather();

            if (weatherForTheGame.equals("storm")) {
                Statistics.incrementStormsCounter();
            } else {
                Knight knight = new Knight(game.getGameJson());
                Dragon dragon = new Dragon(weatherForTheGame, knight.getSkills());
                Fight fight = new Fight(game.getGameId(), dragon.getSkills());
                fight.start();
            }
            fightCounter++;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            break;
        } while (fightCounter < numberOfFights);

        Statistics.calculateStatistics(numberOfFights, fightCounter);
        Statistics.clearCounters();
    }
}
