package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import org.json.JSONObject;

class Game {

    private static Logger log = Logger.getLogger(Game.class);
    private static final String gameEndpoint = "http://www.dragonsofmugloar.com/api/game/";

    private int gameId;
    private JSONObject gameJson;

    void start() throws Exception {
        Request request = new Request(gameEndpoint);

        try {
            gameJson = new JSONObject(request.sendGetRequest());
        } catch (Exception e) {
            throw new Exception("Can't get JSON from GET response.");
        }

        try {
            gameId = gameJson.getInt("gameId");
        } catch (Exception e) {
            throw new Exception("Can't get gameId from JSON.");
        }

        log.info("New game initiated with ID " + gameId);
    }

    JSONObject getGameJson() {
        return gameJson;
    }

    int getGameId() {
        return gameId;
    }
}
