package main.java.com.bigbank.dragonsofmugloar;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class KnightTest {
    @Test
    public void getSkillsFromValidJson() throws Exception {
        String validJson = "{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": {\n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 7,\n" +
                "    \"armor\": 7,\n" +
                "    \"agility\": 3,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}";

        JSONObject json = new JSONObject(validJson);

        String[][] knightArray = new String[][] {
                {"attack","7"},
                {"armor", "7"},
                {"agility", "3"},
                {"endurance", "3"}};

        Knight knight = new Knight(json);

        Assert.assertArrayEquals("Skills should be parsed from JSON", knightArray, knight.getSkills());
    }

    @Test
    public void getSortedSkillsFromJson() throws Exception {
        String validJson = "{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": {\n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 8,\n" +
                "    \"armor\": 4,\n" +
                "    \"agility\": 5,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}";

        JSONObject json = new JSONObject(validJson);

        String[][] knightArray = new String[][] {
                {"attack","8"},
                {"agility", "5"},
                {"armor", "4"},
                {"endurance", "3"}};

        Knight knight = new Knight(json);
        Assert.assertArrayEquals("Skills should be sorted from JSON", knightArray, knight.getSkills());
    }

    @Test(expected = Exception.class)
    public void getSkillsFromInvalidJson() throws Exception {
        String invalidJson = "{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": \n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 8,\n" +
                "    \"armor\": 4,\n" +
                "    \"agility\": 5,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}";

        JSONObject json = new JSONObject(invalidJson);
        new Knight(json);
    }

    @Test
    public void getSortedKnightArray() throws Exception {

        String[][] knightArray = new String[][] {
                {"attack","8"},
                {"agility", "4"},
                {"armor", "3"},
                {"endurance", "5"}};

        String[][] sortedKnightArray = new String[][] {
                {"attack","8"},
                {"endurance", "5"},
                {"agility", "4"},
                {"armor", "3"}};

        JSONObject json = new JSONObject("{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": {\n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 8,\n" +
                "    \"armor\": 4,\n" +
                "    \"agility\": 5,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}");

        Knight knight = new Knight(json);
        Assert.assertArrayEquals("Array are not the same", sortedKnightArray, knight.sort(knightArray));
    }

    @Test
    public void getSortedFromLowestToGreatestKnightArray() throws Exception {

        String[][] knightArray = new String[][] {
                {"attack","2"},
                {"agility", "4"},
                {"armor", "6"},
                {"endurance", "8"}};

        String[][] sortedKnightArray = new String[][] {
                {"endurance","8"},
                {"armor", "6"},
                {"agility", "4"},
                {"attack", "2"}};

        JSONObject json = new JSONObject("{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": {\n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 8,\n" +
                "    \"armor\": 4,\n" +
                "    \"agility\": 5,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}");

        Knight knight = new Knight(json);
        Assert.assertArrayEquals("Array are not the same", sortedKnightArray, knight.sort(knightArray));
    }

    @Test
    public void getSortedFromGreatestToLowestKnightArray() throws Exception {

        String[][] knightArray = new String[][] {
                {"attack","8"},
                {"agility", "6"},
                {"armor", "4"},
                {"endurance", "2"}};

        String[][] sortedKnightArray = new String[][] {
                {"attack","8"},
                {"agility", "6"},
                {"armor", "4"},
                {"endurance", "2"}};

        JSONObject json = new JSONObject("{\n" +
                "  \"gameId\": 3233,\n" +
                "  \"knight\": {\n" +
                "    \"name\": \"Sir. Thomas Walters of Manitoba\",\n" +
                "    \"attack\": 8,\n" +
                "    \"armor\": 4,\n" +
                "    \"agility\": 5,\n" +
                "    \"endurance\": 3\n" +
                "  }\n" +
                "}");

        Knight knight = new Knight(json);
        Assert.assertArrayEquals("Array are not the same", sortedKnightArray, knight.sort(knightArray));
    }
}