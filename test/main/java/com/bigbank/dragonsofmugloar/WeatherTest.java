package main.java.com.bigbank.dragonsofmugloar;

import org.junit.Assert;
import org.junit.Test;

public class WeatherTest {
    @Test
    public void weatherNotEmpty() throws Exception {
        Weather weather = new Weather(222);
        Assert.assertTrue("Weather shouldn't be empty", !weather.getWeather().equals(""));
    }

    @Test
    public void weatherNotNull() throws Exception {
        Weather weather = new Weather(222);
        Assert.assertTrue("Weather shouldn't be null", weather.getWeather() != null);
    }

    @Test
    public void weatherReturnsAllowedValues() throws Exception {
        Weather weather = new Weather(222);
        Assert.assertTrue("Weather should be normal|storm|flood|dry|fog: ", weather.getWeather().matches("(normal|storm|flood|dry|fog)"));
    }

    @Test(expected = Exception.class)
    public void parseEmptyXml() throws Exception {
        Weather weather = new Weather(222);
        weather.parseWeatherFromXml("");
    }

    @Test(expected = Exception.class)
    public void parseInvalidXml() throws Exception {
        Weather weather = new Weather(222);
        weather.parseWeatherFromXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<report>\n" +
                "    <time>Sun Jan 15 2017 15:35:32 GMT+0000 (UTC)</time>\n" +
                "    <coords>\n" +
                "        <x>3916.234</x>\n" +
                "        <y>169.914</y>\n" +
                "        <z>6.33</z>\n" +
                "    <code>NMR</code>\n" +
                "    <message>Another day of everyday normal regular weather, business as usual, unless it’s going to be like the time of the Great Paprika Mayonnaise Incident of 2014, that was some pretty nasty stuff.</message>\n" +
                "    <varX-Rating>8</varX-Rating>\n" +
                "</report>");
    }

    @Test
    public void parseValidXml() throws Exception {
        Weather weather = new Weather(222);
        String parsedWeather = weather.parseWeatherFromXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<report>\n" +
                "    <time>Sun Jan 15 2017 15:35:32 GMT+0000 (UTC)</time>\n" +
                "    <coords>\n" +
                "        <x>3916.234</x>\n" +
                "        <y>169.914</y>\n" +
                "        <z>6.33</z>\n" +
                "    </coords>\n" +
                "    <code>NMR</code>\n" +
                "    <message>Another day of everyday normal regular weather, business as usual, unless it’s going to be like the time of the Great Paprika Mayonnaise Incident of 2014, that was some pretty nasty stuff.</message>\n" +
                "    <varX-Rating>8</varX-Rating>\n" +
                "</report>");

        Assert.assertEquals("Weather should be normal", "normal", parsedWeather);
    }
}