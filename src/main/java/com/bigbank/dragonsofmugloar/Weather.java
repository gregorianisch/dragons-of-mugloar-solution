package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

class Weather {

    private static Logger log = Logger.getLogger(Weather.class);
    private int gameId;

    public Weather(int gameId) {
        this.gameId = gameId;
    }

    String getWeather() throws Exception {

        String weatherEndpoint = "http://www.dragonsofmugloar.com/weather/api/report/" + this.gameId;
        String weather;

        try {
            Request request = new Request(weatherEndpoint);
            String response = request.sendGetRequest();
            weather = parseWeatherFromXml(response);
        } catch (Exception e) {
            throw new Exception("Can't get or parse XML from GET request.");
        }

        log.info("Weather for game with ID " +  this.gameId + " is: " + weather);
        return weather;
    }

    String parseWeatherFromXml(String response) throws Exception {

        String xPathExpressionForWeatherMessage = "/report/message/text()";
        String weather;

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(response)));

        XPath xPath = XPathFactory.newInstance().newXPath();
        XPathExpression expr = xPath.compile(xPathExpressionForWeatherMessage);
        Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);

        String fullMessageFromXml = node.getNodeValue().toLowerCase();

        if (fullMessageFromXml.contains("storm")) {
            weather = "storm";
        } else if (fullMessageFromXml.contains("normal")) {
            weather = "normal";
        } else if (fullMessageFromXml.contains("flood")) {
            weather = "flood";
        } else if (fullMessageFromXml.contains("dry")) {
            weather = "dry";
        } else if (fullMessageFromXml.contains("fog")) {
            weather = "fog";
        } else {
            throw new IllegalArgumentException("Specified weather type not found in this xml: " + fullMessageFromXml);
        }

        return weather;
    }
}
