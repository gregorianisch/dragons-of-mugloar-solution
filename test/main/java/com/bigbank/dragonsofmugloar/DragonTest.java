package main.java.com.bigbank.dragonsofmugloar;

import org.junit.Assert;
import org.junit.Test;
import org.json.JSONObject;

public class DragonTest {
    @Test
    public void getSkillsForNormalWeather() throws Exception {

        JSONObject dragonSkills = new JSONObject();
        JSONObject dragonJson = new JSONObject();

        dragonSkills.put("clawSharpness", 7);
        dragonSkills.put("scaleThickness", 8);
        dragonSkills.put("wingStrength", 2);
        dragonSkills.put("fireBreath", 3);

        dragonJson.put("dragon", dragonSkills);

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("normal", knightArray);

        Assert.assertEquals("JSON should be the same ", dragonJson.toString(), dragon.getSkills().toString());
    }

    @Test
    public void getSkillsForFloodWeather() throws Exception {

        JSONObject dragonSkills = new JSONObject();
        JSONObject dragonJson = new JSONObject();

        dragonSkills.put("clawSharpness", 10);
        dragonSkills.put("scaleThickness", 5);
        dragonSkills.put("wingStrength", 5);
        dragonSkills.put("fireBreath", 0);

        dragonJson.put("dragon", dragonSkills);

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("flood", knightArray);

        Assert.assertEquals("JSON should be the same ", dragonJson.toString(), dragon.getSkills().toString());
    }

    @Test
    public void getSkillsForFogWeather() throws Exception {

        JSONObject dragonSkills = new JSONObject();
        JSONObject dragonJson = new JSONObject();

        dragonSkills.put("clawSharpness", 5);
        dragonSkills.put("scaleThickness", 5);
        dragonSkills.put("wingStrength", 5);
        dragonSkills.put("fireBreath", 5);

        dragonJson.put("dragon", dragonSkills);

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("fog", knightArray);

        Assert.assertEquals("JSON should be the same ", dragonJson.toString(), dragon.getSkills().toString());
    }

    @Test
    public void getSkillsForDryWeather() throws Exception {

        JSONObject dragonSkills = new JSONObject();
        JSONObject dragonJson = new JSONObject();

        dragonSkills.put("clawSharpness", 5);
        dragonSkills.put("scaleThickness", 5);
        dragonSkills.put("wingStrength", 5);
        dragonSkills.put("fireBreath", 5);

        dragonJson.put("dragon", dragonSkills);

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("dry", knightArray);

        Assert.assertEquals("JSON should be the same ", dragonJson.toString(), dragon.getSkills().toString());
    }

    @Test(expected = Exception.class)
    public void getSkillsFromInvalidArray() throws Exception {
        String[][] knightArray = new String[][] {
                {"","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("normal", knightArray);
        dragon.getSkills();
    }

    @Test(expected = Exception.class)
    public void getSkillsFromInvalidWeather() throws Exception {
        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("storm", knightArray);
        dragon.getSkills();
    }

    @Test
    public void skillsTotalAmountIsNotGreaterThan20() throws Exception {

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("normal", knightArray);
        JSONObject dragonSkills = dragon.getSkills().getJSONObject("dragon");
        int total = dragonSkills.getInt("clawSharpness") +
                    dragonSkills.getInt("scaleThickness") +
                    dragonSkills.getInt("wingStrength") +
                    dragonSkills.getInt("fireBreath");

        Assert.assertFalse("Amount of skills should be 20 ", total > 20);
    }

    @Test
    public void skillsTotalAmountIsNotLessThan20() throws Exception {

        String[][] knightArray = new String[][] {
                {"attack","6"},
                {"armor", "8"},
                {"agility", "3"},
                {"endurance", "3"}};

        Dragon dragon = new Dragon("normal", knightArray);
        JSONObject dragonSkills = dragon.getSkills().getJSONObject("dragon");
        int total = dragonSkills.getInt("clawSharpness") +
                dragonSkills.getInt("scaleThickness") +
                dragonSkills.getInt("wingStrength") +
                dragonSkills.getInt("fireBreath");

        Assert.assertFalse("Amount of skills should be 20 ", total < 20);
    }
}