package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;

class Statistics {

    private static Logger log = Logger.getLogger(Statistics.class);

    private static int victoriesCounter = 0;
    private static int stormsCounter = 0;

    static void calculateStatistics(int numberOfFights, int fightCounter) {

        double victoriesRate = 0.0;
        double stormsRate;

        stormsRate = (stormsCounter * 100.0) / numberOfFights;

        if (victoriesCounter > 0) {
            victoriesRate = (victoriesCounter * 100.0) / numberOfFights;
        }

        log.info("Total fights completed: " + fightCounter +
                " with win rate: " + victoriesRate + "%" +
                " (" + victoriesCounter + " wins)" +
                " out of which: " + stormsRate + "% " +
                "(" + stormsCounter + " storms)" +
                " were UNWINNABLE storms.");
    }

    static void incrementVictoriesCounter() {
        victoriesCounter++;
    }

    static void incrementStormsCounter() {
        stormsCounter++;
    }

    static void clearCounters() {
        victoriesCounter = 0;
        stormsCounter = 0;
    }
}
