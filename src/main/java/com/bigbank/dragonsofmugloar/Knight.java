package main.java.com.bigbank.dragonsofmugloar;

import org.apache.log4j.Logger;
import org.json.JSONObject;

class Knight {

    private static Logger log = Logger.getLogger(Knight.class);
    private JSONObject gameJson;

    public Knight(JSONObject gameJson) {
        this.gameJson = gameJson;
    }

    String[][] getSkills() throws Exception {

        String[][] skills;

        JSONObject knightData = gameJson.getJSONObject("knight");

        String[][] knightSkills = new String[][] {
                {"attack", knightData.get("attack").toString()},
                {"agility", knightData.get("agility").toString()},
                {"armor", knightData.get("armor").toString()},
                {"endurance", knightData.get("endurance").toString()}};

        skills = sort(knightSkills);

        log.info("Knight data: " + knightData.toString(4));

        return skills;
    }

    String[][] sort(String[][] skills) {

        for (int i = 0; i < skills.length; i++) {
            for (int j = 0; j < skills.length - 1; j++) {
                if (new Integer(skills[j][1]) < new Integer(skills[j+1][1])) {
                    String[] tmp = skills[j];
                    skills[j] = skills[j+1];
                    skills[j+1] = tmp;
                }
            }
        }

        return skills;
    }
}
